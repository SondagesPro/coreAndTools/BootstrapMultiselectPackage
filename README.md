# BootstrapMultiselectPackage

Add [bootstrap-multiselect](https://github.com/davidstutz/bootstrap-multiselect) package and translation for other plugins.

For LimeSurvey 3 to 5, plugin register [0.9.15](https://github.com/davidstutz/bootstrap-multiselect/releases/tag/v0.9.15) release.

## Sample usage

- Register needed package to be used in Quetsion theme `{{ registerPackage('bootstrap-multiselect-public') }}`
- Check if exist inside plugin `if (Yii::getPathOfAlias('BootstrapMultiselectPackage') { echo "Exist"; }`
- Register package in plugin `Yii::app()->getClientScript()->registerPackage('bootstrap-multiselect-public');`, you can use it as dependency for nyour own package
- Add translation for plugin `\BootstrapMultiselectPackage\Utilities::registerScriptLanguage();`

## Home page & Copyright
- HomePage <https://sondages.pro/>
- Copyright © 2023 Denis Chenu <https://sondages.pro>
- [Support](https://support.sondages.pro)
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro)

This plugin is distributed under [BSD 3 licence](https://opensource.org/license/BSD-3-clause/)
