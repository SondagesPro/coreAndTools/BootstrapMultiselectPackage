<?php

/**
 * bootstrap-multiselect-public add a package bootstrap-multiselect for public survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class BootstrapMultiselectPackage extends PluginBase
{
    /* @inheritdoc */
    protected static $name = 'BootstrapMultiselectPackage';
    /* @inheritdoc */
    protected static $description = 'Add bootstrap-multiselect with way to trabnslate tool for plugin';
    /* @inheritdoc */
    public $allowedPublicMethods = [];

    /* @inheritdoc */
    public function init()
    {
        Yii::setPathOfAlias('BootstrapMultiselectPackage', dirname(__FILE__));
        /* register the package */
        $this->subscribe('beforeSurveyPage');
        /* add message source */
        $this->subscribe('afterPluginLoad');
    }

    /**
     * @see event
     * register the package in survey
     **/
    public function beforeSurveyPage() 
    {
        $this->registerPackage();
    }

    /**
     * Function to register the package if not exist
     **/
    public function registerPackage()
    {
        $debug = boolval(App()->getConfig("debug"));
        $minVersion = ($debug > 0) ? "" : ".min";
        Yii::app()->clientScript->addPackage('bootstrap-multiselect-dist', array(
            'basePath'    => 'BootstrapMultiselectPackage.vendor.bootstrap-multiselect.dist',
            'js'         => array('js/bootstrap-multiselect.js'),
            'css'         => array('css/bootstrap-multiselect.css'),
            'depends'     => array(
                'jquery',
                'bootstrap'
            )
        ));
        Yii::app()->clientScript->addPackage('bootstrap-multiselect-public', array(
            'basePath'    => 'BootstrapMultiselectPackage.assets',
            'js'          => array('bootstrap-multiselect-limesurvey.js'),
            'css'         => array('bootstrap-multiselect-limesurvey.css'),
            'depends'     => array(
                'bootstrap-multiselect-dist'
            )
        ));
    }

    /**
    * Add this translation just after loaded all plugins
    * And set the config if needed
    * @see event afterPluginLoad
    */
    public function afterPluginLoad()
    {
        if (!empty(App()->getComponent('Messages' . 'BootstrapMultiselectPackage'))) {
            return;
        }
        $message = Yii::app()->getComponent('messages');
        // messageSource for this plugin:
        $messageSource = array(
            'class' => 'CGettextMessageSource',
            'cacheID' => 'BootstrapMultiselectPackage' . 'Lang',
            'cachingDuration' => 1, //$message->cachingDuration,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR . 'locale',
            'catalog' => 'messages',// default from Yii
        );
        Yii::app()->setComponent('Messages' . 'BootstrapMultiselectPackage' , $messageSource);
    }
}
