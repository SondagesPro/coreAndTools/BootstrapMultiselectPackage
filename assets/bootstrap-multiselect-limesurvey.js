!(function ($, templates) {
    templates = $.extend(templates,  
        {
            "filter" : "<li class=\"multiselect-item filter\"><div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-search\" aria-hidden=\"true\"><\/i><\/span><input class=\"form-control multiselect-search\" type=\"text\"><\/div><\/li>",
            "filterClearBtn" : "<span class=\"input-group-btn\" aria-hidden=\"true\"><button class=\"btn btn-default multiselect-clear-filter\" type=\"button\"><i class=\"fa fa-times-circle\"><\/i><\/button><\/span>"
        }
    )
}(jQuery, jQuery.fn.multiselect.Constructor.prototype.defaults.templates));
