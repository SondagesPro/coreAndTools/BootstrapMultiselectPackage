<?php

/**
 * This file is par of BootstrapMultiselectPackage plugin
 * @license AGPL v3
 * @since 0.1.0
 */

namespace BootstrapMultiselectPackage;

use App;
use CClientScript;

Class Utilities
{
    /**
     * registering the language script
     * @return void
     */
    public static function registerScriptLanguage()
    {
        $language = [
            'allSelectedText' => self::translate('All selected'),
            'filterPlaceholder' => self::translate('Search'),
            'nSelectedText' => self::translate('selected'),
            'nonSelectedText' => self::translate('None selected'),
            'selectAllText' => self::translate(' Select all'),
        ];
       $script = "!(function ($, defaults) {\n";
       $script .= "    defaults = $.extend(defaults,  \n";
       $script .= "        " . json_encode($language) . "\n";
       $script .= "    )\n";
       $script .= "}(jQuery, jQuery.fn.multiselect.Constructor.prototype.defaults));\n";
       App()->getClientScript()->registerScript('BootstrapMultiselectPackageLanguage', $script, CClientScript::POS_HEAD);
    }

    /**
     * @deprecated : inlcude in package
     * @return void
     */
    public static function registerScriptTemplate()
    {
        return;
    }

    /**
     * Translation for plugin
     *
     * @param string $sToTranslate The message that are being translated
     * @param string $sEscapeMode
     * @param string $sLanguage
     * @return string
     */
    public static function translate($string, $escapemode = 'unescaped', $language = null)
    {
        $coreTranslation = \Yii::t(
            '',
            $string,
            array(),
            null,
            $language
        );
        if ($coreTranslation != $string) {
            /* Translated via core : DB translation */
            return \quoteText($coreTranslation, $escapemode);
        }
        return \quoteText(
            \Yii::t(
                '',
                $string,
                array(),
                'Messages' . 'BootstrapMultiselectPackage',
                $language
            ),
            $escapemode
        );
    }
}
